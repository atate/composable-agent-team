# Composable Agent Team

## Intro

The cmoposable agent team is a way of simplifying advaned AI agents to solve complex tasks. It consists of the following advanced Features

Breakdown of a complex task into component sub-tasks
COnfiguration of specialist Agents that can be assigned sub-tasks
Recursion until the task is solved satisfactorily
QA to make sure that agents are solving the correct sub-tasks effectively
Easy reconfiguration to assess if team architecture affects performance
Polling and error reporting so that agent failures do not result in the job dying

## Agent teams

Agent Teams are collections of AI agents that collectively solve a complex task. ANy number of Agents can be provided with any speciality. The basic design principle should always be that the minimal team to solve the task should be formed, but we do not often understand the best way to configure a team of collaborators (either human or AI). The Agent team will be executed from a python code that uses API calls to construct, instruct, communicate with and guide agents in order to solve the complex task. 

## Advisor Agents

Advisor Agents are used to help condigure the Agent Team but are not used inside any Agent teams. They include

Agent Team Coding Expert: Helps the user to write codes that use the Agent Team
Agent Team Designer: An expert in the Agent Team concept that will design a configuration to the Agent team
Agent Task Solver: Will break down a tast into components that can be solved by different members of the Agent team

## Agents

An agent is a configured AI endpoint that can be employed to solve a task. The defining characteristics of an Agent are:

Instructions: the text instructions that 
Functions   : Any structured input/output that the agents may need or provide
Tools       : capabilities of the agent that must be stated in advance

Agents can develop capabilities over time, and recieve a satisfaction rating from their peers on their ability to solve complex tasks. 



